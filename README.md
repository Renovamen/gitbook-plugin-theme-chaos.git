# GitBook Chaos Theme

A chaos theme for GitBook. Forked from [GitbookIO/theme-default](https://github.com/GitbookIO/theme-default), inspired by [evalor/theme-evalor](https://github.com/evalor/theme-evalor).

&nbsp;

## Usage

Add it to your `book.json` configuration:

```json
"plugins": [
    "theme-chaos"
]
```